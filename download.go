package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/validation"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

/* Main download window */
func downloadScreen(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewLabelWithStyle("This section provides various options \n for downloading profile photos.", fyne.TextAlignCenter, fyne.TextStyle{}),
	)
}

/* Main one number window */
func oneNumberScreen(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox()
}

/* Download window for one number input */
func makeOneNumberInput(_ fyne.Window) fyne.CanvasObject {
	entry := &widget.Entry{Validator: validation.NewRegexp(`\d`, "")}
	entry.SetPlaceHolder("Must contain a GG number between 1 and 99999999")

	return container.NewVBox(
		widget.NewLabelWithStyle("Input one GG number", fyne.TextAlignCenter, fyne.TextStyle{}),
		entry,
		layout.NewSpacer(),
		widget.NewLabelWithStyle("Click button below to download profile photo", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewButtonWithIcon("Button", theme.DownloadIcon(), func() {
			fmt.Println("Button was tapped")
			convEntry := convStrToUint64(entry.Text)
			oneNumberInput(convEntry)
		}),
		layout.NewSpacer(),
	)
}

/* Download window for one number from file */
func makeOneNumberFromFile(win fyne.Window) fyne.CanvasObject {
	entry := widget.NewEntry()
	entry.SetPlaceHolder("Input full path to file")

	return container.NewVBox(
		// Dialog
		/*widget.NewButton("File Open With Filter (.json)", func() {
			fd := dialog.NewFileOpen(func(JSONreader fyne.URIReadCloser, err error) {
				if err == nil && JSONreader == nil {
					return
				}
				if err != nil {
					dialog.ShowError(err, win)
					return
				}

				//oneNumberFromFile(JSONreader)
			}, win)
			fd.SetFilter(storage.NewExtensionFileFilter([]string{".json"}))
			fd.Show()
		})*/
		// Input path
		widget.NewLabelWithStyle("Input path to JSON file", fyne.TextAlignCenter, fyne.TextStyle{}),
		entry,
		layout.NewSpacer(),
		widget.NewLabelWithStyle("Click button below to start", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewButtonWithIcon("Button", theme.DownloadIcon(), func() {
			fmt.Println("Buttton was tapped")
			oneNumberFromFile2(entry.Text)
		}),
		layout.NewSpacer(),
	)
}

/* Download window for random number */
func makeRandNumber(_ fyne.Window) fyne.CanvasObject {

	return container.NewVBox(
		widget.NewLabelWithStyle("Generate random GG number and download", fyne.TextAlignCenter, fyne.TextStyle{}),
		layout.NewSpacer(),
		widget.NewLabelWithStyle("Click button below to generate random \n number and download profile photo", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewButtonWithIcon("Button", theme.DownloadIcon(), func() {
			fmt.Println("Button was tapped")
			randNumber()
		}),
		layout.NewSpacer(),
	)
}

/* Main range numbers window */
func rangeNumbersScreen(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewLabelWithStyle("This section can lead to server problems (DoS) \n and expose you to law consequences.", fyne.TextAlignCenter, fyne.TextStyle{}),
	)
}

/* Predefined range numbers download window */
func makeRangeNumbers1(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewLabelWithStyle("", fyne.TextAlignCenter, fyne.TextStyle{}),
		layout.NewSpacer(),
		widget.NewLabelWithStyle("Click button below to start downloading", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewButtonWithIcon("Button", theme.DownloadIcon(), func() {
			fmt.Println("Button was tapped")
			prefRangeNumbers()
		}),
		layout.NewSpacer(),
	)
}

/* Custom number ranges download window */
func makeRangeNumbers2(_ fyne.Window) fyne.CanvasObject {
	entryStart := &widget.Entry{Validator: validation.NewRegexp(`\d`, "")}
	entryStart.SetPlaceHolder("Start range (0 is min)")
	entryEnd := &widget.Entry{Validator: validation.NewRegexp(`\d`, "")}
	entryEnd.SetPlaceHolder("End range (99999999 is max)")

	return container.NewVBox(
		widget.NewLabelWithStyle("", fyne.TextAlignCenter, fyne.TextStyle{}),
		layout.NewSpacer(),
		widget.NewLabelWithStyle("Below you can set your range", fyne.TextAlignCenter, fyne.TextStyle{}),
		entryStart,
		entryEnd,
		widget.NewButton("Click to set range and start", func() {
			fmt.Println("Button was tapped")
			convEntryStart := convStrToUint64(entryStart.Text)
			convEntryEnd := convStrToUint64(entryEnd.Text)
			rangeNumbers(convEntryStart, convEntryEnd)
		}),
		layout.NewSpacer(),
	)
}

/* Main all numbers window */
func allNumbersScreen(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewLabelWithStyle("This section can lead to server problems (DoS) \n and expose you to law consequences.", fyne.TextAlignCenter, fyne.TextStyle{}),
	)
}

/* Predefined delays of all numbers download window */
func makeAllNumbers1(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewLabelWithStyle("Download profile photo from all numbers (from 1 to 99999999)", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewLabelWithStyle("With predefined delay", fyne.TextAlignCenter, fyne.TextStyle{Bold: true}),
		layout.NewSpacer(),
		widget.NewButton("Click to start", func() {
			fmt.Println("Button was tapped")
			allNumbersPrefDelay()
		}),
		layout.NewSpacer(),
	)
}

/* Custom delay download window for all numbers */
func makeAllNumbers2(_ fyne.Window) fyne.CanvasObject {
	entry := &widget.Entry{Validator: validation.NewRegexp(`\d`, "")}
	entry.SetPlaceHolder("Must contain number")

	return container.NewVBox(
		widget.NewLabelWithStyle("Download profile photo from all numbers (from 1 to 99999999)", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewLabelWithStyle("With your delay in millisecond", fyne.TextAlignCenter, fyne.TextStyle{Bold: true}),
		layout.NewSpacer(),
		entry,
		widget.NewButton("Click to set delay and start", func() {
			fmt.Println("Button was tapped")
			convEntry := convStrToUint64(entry.Text)
			allNumbersSetDelay(convEntry)
		}),
		layout.NewSpacer(),
	)
}

/* All numbers download window */
func makeAllNumbers3(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewLabelWithStyle("Download profile photo from all numbers (from 1 to 99999999)", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewLabelWithStyle("Without any delay", fyne.TextAlignCenter, fyne.TextStyle{Bold: true}),
		layout.NewSpacer(),
		widget.NewButton("Click to start", func() {
			fmt.Println("Button was tapped")
			allNumbers()
		}),
		layout.NewSpacer(),
	)
}
