# GG_Images
> GG profile photo downloader

[![Release](https://img.shields.io/badge/release-v0.2-cyan)](https://codeberg.org/nanoory/GG_Images/releases)
![Go](https://img.shields.io/badge/Go-v1.15-blue)
[![Go Report Card](https://img.shields.io/badge/go_report-A+-green)](https://goreportcard.com/report/codeberg.org/nanoory/GG_Images)
[![License](https://img.shields.io/badge/GPL-3.0_only-green)](https://opensource.org/licenses/GPL-3.0)

## Table of content
* [About](#about)
* [Screenshots](#screenshots)
* [Features](#features)
* [Operating systems](##operating_systems)
* [Built with](##built_with)
* [Building](#building)
* [TODO](#todo)
* [Licensing](#licensing)


# About
> About the project

The application was created to allow anyone to easily download profile pictures from GG.

The profile picture of each user is available for everyone without the need to log in to the website.

Therefore, the application does not use any GG vulnerabilities, but only allows easier access to publicly available data.

Anyone can easily manually download a profile picture of any GG user by entering the link shown below in the address bar of the web browser
https://avatars.gg.pl/user,GG_number/s,800x800?default=https://www.gg.pl/images/sr-avatar-blank-male-80.png
or
https://avatars.gg.pl/user,GG_number/
and changing the value of GG_number to a specific GG user number.

Please note that some application features may lead to server problems and may expose you to law consequences.

The application should be considered as developed for educational purposes only.

# Screenshots
Dark theme
![dark theme](screenshots/screenshot_dark.png "Dark theme")

Light theme
![light theme](screenshots/screenshot_light.png "Light theme")

# Features

- Download profile photo from:
-- One number
-- Random number
-- Range numbers
-- All numbers

## Operating systems
> Program tests on operating systems and availability in the official repository

| Operating system | Tested | Repository | Link |
| --- | --- | --- | --- |
| Debian Bullseye | Yes |
| RebornOS | Yes |
| BlackArch | Yes | Yes | https://blackarch.org/tools.html |

## Built with

- Go 1.15


# Building
> How to build

```
git clone https://codeberg.org/nanoory/GG_Images
go build *.go
```

# TODO:

- [x] Base functions
- [] Read GG numbers from JSON file
- [] Progress bars for download windows

# Licensing
GPL-3.0 only
