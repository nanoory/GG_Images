package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

/* JSON entries type */
type JSONuser struct {
	Number       uint32
	Nick         string
	Gender       string
	Birthday     string
	City         string
	Description  string
	First_name   string
	Second_name  string
	Last_name    string
	Email        string
	Country_code uint8
	Phone_number uint32
	Note         string
}

/* Variables */
var (
	jUsers   []JSONuser
	GGnumber string
	fileName string
)

/**/
func randNumber() {
	rand.Seed(time.Now().UnixNano())
	randGGnumber := rand.Intn(99999999)
	GGnumber = convIntToStr(randGGnumber)
	fileName = GGnumber + ".png"
	FORdownloadFile(fileName, GGnumber)
	fmt.Println("## File name is: " + fileName + "    and    " + "## GG number is: " + GGnumber)
}

/**/
func oneNumberInput(input uint64) {
	//
	if input <= 99999999 {
		GGnumber = convUint64ToStr(input)
		fileName = GGnumber + ".png"
		FORdownloadFile(fileName, GGnumber)
		fmt.Println("## File name is: " + fileName + "    and    " + "## GG number is: " + GGnumber)
	}
}

/**/
/*func oneNumberFromFile(file fyne.URIReadCloser) {
	// Read JSON file
}*/

/**/
func oneNumberFromFile2(input string) {
	file, err := ioutil.ReadFile(input)
	if err != nil {
		log.Fatal(err)
	}

	json.Unmarshal([]byte(file), &jUsers)
	fmt.Printf("\nJSON file: %+v\n", jUsers)
	// Print raw text
	rawText := string(file)
	fmt.Println("\nRaw text is:\n" + rawText)
	// Test
}

/**/
func rangeNumbers(startRange uint64, endRange uint64) {
	var (
		iStart = startRange
		iEnd   = endRange
	)
	// Check if end of range is less than 99999999
	if iEnd <= 99999999 {
		for ; iStart <= iEnd; iStart++ {
			GGnumber = convUint64ToStr(iStart)
			fileName = GGnumber + ".png"
			FORdownloadFile(fileName, GGnumber)
			fmt.Println("## File name is: " + fileName + "    and    " + "## GG number is: " + GGnumber)
		}
	}
}

/**/
func prefRangeNumbers() {
	for i := 60000000; i <= 80000000; i++ {
		GGnumber = convIntToStr(i)
		fileName = GGnumber + ".png"
		FORdownloadFile(fileName, GGnumber)
		fmt.Println("## File name is: " + fileName + "    and    " + "## GG number is: " + GGnumber)
	}
}

/**/
func allNumbers() {
	for i := 1; i <= 99999999; i++ {
		GGnumber = convIntToStr(i)
		fileName = GGnumber + ".png"
		FORdownloadFile(fileName, GGnumber)
		fmt.Println("## File name is: " + fileName + "    and    " + "## GG number is: " + GGnumber)
	}
}

/**/
func allNumbersPrefDelay() {
	for i := 1; i <= 99999999; i++ {
		GGnumber = convIntToStr(i)
		fileName = GGnumber + ".png"
		FORdownloadFile(fileName, GGnumber)
		fmt.Println("## File name is: " + fileName + "    and    " + "## GG number is: " + GGnumber)
		time.Sleep(5 + time.Millisecond)
	}
}

/**/
func allNumbersSetDelay(input uint64) {
	for i := 1; i <= 99999999; i++ {
		GGnumber = convIntToStr(i)
		fileName = GGnumber + ".png"
		FORdownloadFile(fileName, GGnumber)
		fmt.Println("## File name is: " + fileName + "    and    " + "## GG number is: " + GGnumber)
		time.Sleep(time.Duration(uint64(time.Millisecond) * uint64(input)))
	}
}

/**/
func FORdownloadFile(fileName string, GGnumber string) {
	URL := "https://avatars.gg.pl/user," + GGnumber + "/s,800x800?default=https://www.gg.pl/images/sr-avatar-blank-male-80.png"
	fmt.Println("URL is: " + URL)
	downloadFile(URL, fileName)
	fmt.Printf("File %s downlaod in current working directory", fileName)
}

/**/
func downloadFile(URL, fileName string) error {
	// Get the response bytes from the url
	response, err := http.Get(URL)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return errors.New("Received non 200 response code")
	}
	// Create a empty file
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	// Write the bytes to the fiel
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	return nil
}
