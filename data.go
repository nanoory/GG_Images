package main

import (
	"fyne.io/fyne/v2"
)

// Window defines the data structure for a window
type Window struct {
	Title, Intro string
	View         func(w fyne.Window) fyne.CanvasObject
}

var (
	// Windows defines the metadata for each window
	Windows = map[string]Window{
		"w_welcome": {"Welcome",
			"",
			welcomeScreen,
		},
		"w_about": {"About",
			"Somethink about the app",
			aboutScreen,
		},
		"m_download": {"Download",
			"Download section",
			downloadScreen,
		},
		"m_oneNumber": {"One number",
			"One number section",
			oneNumberScreen,
		},
		"w_oneNumberInput": {"Enter number",
			"Profile photo from one number",
			makeOneNumberInput,
		},
		"w_oneNumberFromFile": {"From file",
			"Profile photo from number from file",
			makeOneNumberFromFile,
		},
		"w_randNumber": {"Random number",
			"Profile photo from random number",
			makeRandNumber,
		},
		"m_rangeNumbers": {"Range numbers",
			"Range numbers section",
			rangeNumbersScreen,
		},
		"w_rangeNumbers1": {"Predefined range",
			"Profile photos from predefined range",
			makeRangeNumbers1,
		},
		"w_rangeNumbers2": {"Custom range",
			"Profile photos from your own custom range",
			makeRangeNumbers2,
		},
		"m_allNumbers": {"All numbers",
			"All numbers section",
			allNumbersScreen,
		},
		"w_allNumbers1": {"Predefined delay",
			"Profile photos from all numbers with predefined delay",
			makeAllNumbers1,
		},
		"w_allNumbers2": {"Custom delay",
			"Profile photos from all numbers with your own custom delay",
			makeAllNumbers2,
		},
		"w_allNumbers3": {"Without delay",
			"Profile photos from all numbers",
			makeAllNumbers3,
		},
		"m_testing": {"Testing",
			"Testing functions section",
			testingScreen,
		},
		"w_testingSomething": {"Testing Something",
			"",
			makeTestingSomething,
		},
	}

	// WindowIndex  defines how our windows should be laid out in the index tree
	WindowIndex = map[string][]string{
		"":               {"w_welcome", "w_about", "m_download", "m_testing"},
		"m_download":     {"m_oneNumber", "w_randNumber", "m_rangeNumbers", "m_allNumbers"},
		"m_oneNumber":    {"w_oneNumberInput" /*,"w_oneNumberFromFile"*/},
		"m_rangeNumbers": {"w_rangeNumbers1", "w_rangeNumbers2"},
		"m_allNumbers":   {"w_allNumbers1", "w_allNumbers2", "w_allNumbers3"},
		"m_testing":      {"w_testingSomething", "w_oneNumberFromFile"},
	}
)
