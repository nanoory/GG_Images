module codeberg.org/nanoory/GG_Images

go 1.15

require (
	fyne.io/fyne/v2 v2.0.3
	github.com/go-gl/gl v0.0.0-20210501111010-69f74958bac0 // indirect
	github.com/srwiley/oksvg v0.0.0-20210519022825-9fc0c575d5fe // indirect
	github.com/srwiley/rasterx v0.0.0-20210519020934-456a8d69b780 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/image v0.0.0-20210607152325-775e3b0c77b9 // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
