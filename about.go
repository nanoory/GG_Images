package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

/* Main window about */
func aboutScreen(_ fyne.Window) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewLabelWithStyle("The application is not intended for the illegal purposes.", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewLabelWithStyle("It is made for educational purposes only \n and everyone uses it at their own risk.", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewLabelWithStyle("The application also does not use any GG vulnerabilities. ", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewLabelWithStyle("It only takes advantage of the fact that every profile picture \n are publicly available for everyone without logging in.", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewLabelWithStyle("Just click the link below to open in your browser to access the profile picture. \n Only change value of 'GG_number' to a real GG number.", fyne.TextAlignCenter, fyne.TextStyle{}),
		widget.NewHyperlink("avatars.gg.pl", parseURL("https://avatars.gg.pl/user,GG_number/s,800x800?default=https://www.gg.pl/images/sr-avatar-blank-male-80.png")),
	)
}
