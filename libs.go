package main

import (
	_ "errors"
	"fmt"
	"net/url"
	"strconv"

	"fyne.io/fyne/v2"
)

/* Convert String to Int64 */
func convStrToInt64(str string) (value int64) {
	var (
		base = 10
		size = 64
	)

	//str = "12345" // Example
	value, err := strconv.ParseInt(str, base, size)
	if err == nil {
		fmt.Println("Parse was correct")
		fmt.Println("Int from String is: ", value)
		return value
	} else {
		fmt.Println("Parse has error: ", err)
		fmt.Println("Uint from String is: ", value)
		return value
	}
}

/* Convert Int to String */
func convIntToStr(value int) (str string) {
	//var ()

	//value = 12 // Example
	str = strconv.Itoa(value)
	fmt.Println("String from Int is: " + str)
	return str
}

/* Convert String to Uint64 */
func convStrToUint64(str string) (value uint64) {
	var (
		base = 10
		size = 64
	)

	//str = "1442313" // Example
	value, err := strconv.ParseUint(str, base, size)
	if err == nil {
		fmt.Println("Parse was correct")
		fmt.Println("Uint from String is: ", value)
		return value
	} else {
		fmt.Println("Parse has error: ", err)
		fmt.Println("Uint from String is: ", value)
		return value
	}
}

/* Convert Uint64 to String */
func convUint64ToStr(value uint64) (str string) {
	//var ()

	//value = 2142145954389789587 // Example
	str = strconv.FormatUint(value, 10)
	//fmt.Println("Parse was correct")
	fmt.Println("String from Uint64 is: " + str)
	return str
}

/**/
func testAllConv() {
	// Test var
	var (
		str       string = "412673426"
		intValue  int    = 3424
		uintValue uint64 = 65342983
	)
	// Tests func
	convStrToInt64(str)
	convIntToStr(intValue)
	convStrToUint64(str)
	convUint64ToStr(uintValue)
}

/**/
func parseURL(urlStr string) *url.URL {
	link, err := url.Parse(urlStr)
	if err != nil {
		fyne.LogError("Could not parse URL", err)
	}

	return link
}
